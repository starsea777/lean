package main

import (
	"fmt"
	"net/http"
	"os"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, World!")
}

func main() {
	port := os.Getenv("LEANCLOUD_APP_PORT")
	if port == "" {
		port = "80"
	}

	// 注册处理函数
	http.HandleFunc("/", helloHandler)
	http.HandleFunc("/hello", helloHandler)

	// 启动 HTTP 服务器，监听端口 8080
	fmt.Println("Server is listening on port" + port)
	http.ListenAndServe(":"+port, nil)
}
